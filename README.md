# To do list Api

API Rest para aplicación de to do list cooperativa
Desarrollado con NodeJs y MongoDB

## Documentación 

```
https://documenter.getpostman.com/view/10800617/T17Gennv?version=latest
```
## Descripción  🚀

Aplicación de to do list cooperativa, permite a los usuario crear tareas personales, crear equipos o unirse a ellos, en estos equipos se pueden agregar tareas que pueden ser asignadas a otros usuarios

### Instalación 🔧

```
npm install
```
```
Modificar .env.example a .env
```
```
nodemon app.js
```