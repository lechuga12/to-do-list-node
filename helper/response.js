exports.response = function(data,message='OK',code=200){
    return {
        data,
        message,
        errors: {},
        code
    }
}

exports.bad_request = function(errors={},message='Bad request',code=400){
    return {
        data:{},
        message,
        errors,
        code
    }
}
exports.internal_error = function(message=''){
    return {
        data:{},
        message,
        errors:{},
        code: 500
    }
}

exports.not_found = function(message='Not found'){
    return {
        message,
        data:{},
        errors:{},
        code: 404
    }
}