'user strict';
require('dotenv').config()
const mongoose = require('mongoose');

const user = process.env.DB_USER
const password = process.env.DB_PASSWORD
const db_name = process.env.DB_NAME

const DB_URI = `mongodb+srv://${user}:${password}@cluster0.o2aat.mongodb.net/${db_name}?retryWrites=true&w=majority`

mongoose.connect(DB_URI,{
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err) => {
    if (err) throw err
    console.log('Mongodb is connected')
})

module.exports = mongoose;
