const express = require('express');
const team = express.Router();

const teamsController = require('../controllers/teamsController');

team.get('/', teamsController.get_teams);
team.post('/', teamsController.post_team);
team.put('/:id',teamsController.put_team);
team.get('/:id',teamsController.get_team)
team.delete('/:id',teamsController.delete_team);
team.get('/:id/members', teamsController.get_team_users);
team.post('/:id/members', teamsController.add_user);
team.get('/:id/tasks', teamsController.get_team_tasks);
team.delete('/:id_team/members/:id_user', teamsController.delete_team_user);


module.exports = team