const express = require('express');
const user = express.Router();

const usersController = require('../controllers/usersController');


user.get('/',usersController.get_users);
user.post('/', usersController.post_user);
user.get('/:id', usersController.get_user);
user.put('/:id',usersController.put_user);
user.delete('/:id', usersController.delete_user);
user.get('/:id/tasks', usersController.get_user_tasks);
user.get('/:id/teams', usersController.get_user_team);

module.exports = user