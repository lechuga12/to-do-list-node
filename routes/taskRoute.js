const express = require('express');
const task = express.Router();

const tasksController = require('../controllers/tasksController');

task.get('/',tasksController.get_tasks);
task.post('/', tasksController.post_tasks);
task.get('/:id',tasksController.get_task);
task.put('/:id',tasksController.put_task);
task.delete('/:id', tasksController.delete_task);


module.exports = task