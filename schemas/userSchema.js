const mongoose = require('mongoose');
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = new Schema({
    nickname:{
        type:String,
        required: [true,'Nickname is required'],
        unique: true
    },
    password:{
        type:String,
        required: [true,'Password is required']
    },
    teams:[{
        type: Schema.Types.ObjectId,
        ref: 'Team'
    }]
});

userSchema.plugin(uniqueValidator)

const User = mongoose.model('User', userSchema);

module.exports = User;