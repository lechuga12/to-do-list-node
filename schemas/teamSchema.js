const mongoose = require('mongoose');
const { model } = require('./userSchema');
const Schema = mongoose.Schema

// code -> uuid debe estar por default ?
const teamSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    code:{
        type:String,
        required:true
    },
    members:[{
        type: Schema.Types.ObjectId,
        ref:'User'
    }]
})

const Team = mongoose.model('Team',teamSchema);


module.exports = Team;