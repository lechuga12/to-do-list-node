const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const taskSchema = new Schema({
    content: {
        type:String,
        required: true
    },
    finalized: {
        type:Boolean,
        default: false
    },
    created:{
        type:Date,
        default: Date.now
    },
    author:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required: true
    },
    assigned:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required: false,
        default: null
    },
    team:{
        type: Schema.Types.ObjectId,
        ref:'Team',
        required:false,
        default: null
    }
})

const Task = mongoose.model('Task', taskSchema);
module.exports = Task