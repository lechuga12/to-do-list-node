const Team = require('../schemas/teamSchema');
const User = require('../schemas/userSchema');
const { response, bad_request, internal_error, not_found} = require('../helper/response');
const Task = require('../schemas/taskSchema');

const uuidv1 = require('uuid').v1;

const get_teams = async (req,res) => {
    try{    
        let teams = await Team.find()
        return res.status(200).json(response(teams))

    }catch(e){
        return res.status(500).json(internal_error())
    }
}

const get_team = async (req,res)=>{
    let id_team = req.params.id;
    try {
        let team = await Team.findById(id_team)
        if(!team){
            return res.status(404).json(not_found('Team not found'))
        }
        return res.status(200).json(response(team))
    } catch (err) {
        console.log(err)
        return res.status(500).json( internal_error() )
    }
}

const post_team = async (req,res) => {
    const {
        name,
        id_user
    } = req.body;

    try{
        let user = await User.findById(id_user);
        if(!user){
            return res.status(404).json(not_found('User not found'))
        }
        let code = uuidv1();

        let team = await Team.create({
            name,
            code,
            members: id_user
        })
        user.teams.push(team)
        await user.save()
        return res.status(201).json(response(team))

    }catch(err){
        console.log(err)
        return res.status(500).json( internal_error() )
    }
}

const put_team = async(req,res)=>{
    let id_team = req.params.id;
    const { name } = req.body;
    if(!name){
        return res.status(400).json(bad_request())
    }
    try{
        let team = await Team.findOneAndUpdate({_id:id_team},{name},{new:true});
        if(!team){
            return res.status(404).json(not_found('Team not found'))
        }
        return res.status(200).json(response(team));

    }catch(err){
        console.log(err)
        return res.status(500).json(internal_error())
    }
}
const get_team_users = async (req,res) => {
    let id_team = req.params.id
    try{
        let members = await Team.findOne({_id:id_team},'members')
        return res.status(200).json(response(members))
    }catch(e){
        return res.status(500).json(internal_error())
    }
}

const add_user = async(req, res) => {
    const id_team = req.params.id
    const {id_user} = req.body
    try {
        let team = await Team.findById(id_team)
        if(!team){
            return res.status(404).json(not_found('Team not found'))
        }
        let user = await User.findById(id_user)
        if(!user){
            return res.status(404).json(not_found('User not found'))
        }
        team.members.push(user)
        user.teams.push(team)
        
        await team.save()
        await user.save()

        return res.status(200).json(response(team))
    } catch (err) {
        console.log(err)
        return res.status(500).json(internal_error());
    }
}

const get_team_tasks = async(req, res) => {
    const id_team = req.params.id;
    try{    
        let team = await Team.findById(id_team);
        if(!team){
            return res.status(404).json(not_found('Team not found'))
        }
        console.log(team)
        let tasks = await Task.find({team: team._id});
        return res.status(200).json(response(tasks))
    }catch(err){
        console.log(err)
        return res.status(500).json(internal_error())
    }
}

const delete_team_user = async (req,res)=> {
    const id_team = req.params.id_team;
    const id_user = req.params.id_user

    try{
        let user = await User.findById(id_user);
        if(!user){
            return res.status(404).json(not_found('User not found'));
        }
        let team = await Team.findByIdAndUpdate({_id:id_team},{ $pull: {members: id_user }}, {new:true})
        if(!team){
            return res.status(404).json(not_found('Team not found'));
        }
        // Eliminar el team del user
        await user.updateOne({ $pull: {teams: id_team }} )

        if(team.members.length <= 0){
            await Team.deleteOne({_id: team._id})
            return res.status(200).json(response({},'Member and Team deleted'))
        }
        return res.status(200).json(response(team,'Member deleted'))
        
    }catch(err){
        console.log(err)
        return res.status(500).json(internal_error())
    }
}

const delete_team = async(req, res)=>{
    const id_team = req.params.id
    try{
        let team = await Team.findByIdAndDelete(id_team);
        if(!team){
            return res.status(404).json(not_found('Team not found'))
        }
        // Eliminar el team de todos los usuarios
        await User.updateMany({teams: id_team}, {$pull: {teams:id_team}})
        return res.status(200).json(response(team));
    }catch(err){
        console.log(err)
        return res.status(500).json(internal_error())
    }
}



module.exports = {
    post_team,
    get_teams,
    get_team_users,
    add_user,
    get_team_tasks,
    delete_team_user,
    delete_team,
    put_team,
    get_team
}