const Task = require('../schemas/taskSchema');
const User = require('../schemas/userSchema');
const Team = require('../schemas/teamSchema');
const { response, bad_request, internal_error, not_found} = require('../helper/response');


const get_tasks = async (req,res) => {
    try{
        let tasks = await Task.find();    
        return res.status(200).json(response(tasks))
    }catch(err){
        return res.status(500).json(internal_error());
    }
}

const post_tasks = async (req, res) => {
    const {
        author,
        assigned:id_assigned,
        team:id_team
    } = req.body
    try{
        let user = await User.findById( author )
        if(!user){
            return res.status(404).json(not_found('User not found'));
        }
        // Cuando se agrega en un equipo y se asigna otro usuario
        if(id_team){
            let team = await Team.findById(id_team)
            if(!team){
                return res.status(404).json(not_found('Team not found'));
            }
            if(id_assigned){
                let user_assigned = await User.findById(id_assigned)
                if(!user_assigned){
                    return res.status(404).json(not_found('User assigned not found'));
                }
            }
        }
        let task = await Task.create(req.body)
        return res.status(201).json(response(task))
    }catch(err){
        return internal_error()
    }
}

const get_task = async(req, res) => {
    const id_team = req.params.id;
    try {
        let task = await Task.findById(id_team);
        if(!task){
		    return res.status(404).json(not_found('Task not found'));
        }
        return res.status(200).json(response(task))
    } catch (error) {
        
    }

}

const put_task = async(req,res)=>{
    const id_task = req.params.id;
    try{
        let task = await Task.findOneAndUpdate({_id: id_task},req.body,{new:true});
       	if(!task){
		    return res.status(404).json(not_found('Task not found'));
        }
        return res.status(200).json(response(task))
	}catch(err){
        console.log(err)
        return res.status(500).json(internal_error())
    }
}

const delete_task = async(req, res) => {
    const id_team = req.params.id;
    try {
        let task = await Task.findByIdAndDelete(id_team);
        if(!task){
            return res.status(404).json(not_found('Task not found'))
        }
        return res.status(200).json(response(task,'Task removed'))
    } catch (error) {
        console.log(err)
        return res.status(500).json(internal_error())
    }
}

module.exports = {
    get_tasks,
    post_tasks,
    put_task,
    get_task,
    delete_task
}
