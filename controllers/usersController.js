const User = require('../schemas/userSchema');
const { response, bad_request, internal_error, not_found} = require('../helper/response');
const Task = require('../schemas/taskSchema');
const Team = require('../schemas/teamSchema');

const get_users = async (req,res) => {
    try{
        users = await User.find()
        return res.status(200).json( response(users))
    }catch(err){
        console.log(err)
        return res.status(500).internal_error()
    }
} 
const post_user = async (req,res) => {
    const { nickname, password } = req.body;

    try{
        let user = new User({
            nickname,
            password
        })
        await user.save();
        return res.status(201).json( response(user))

    }catch(err){
        return res.status(400).json( bad_request(err.errors) )
    }
}

const put_user = (req,res) => {
    const id_user = req.params.id
    const {nickname} = req.body
    User.findByIdAndUpdate(id_user,{nickname},{new:true}, (err, user) => {
        if(err){
            console.log(err)
            return res.status(500).json( internal_error())
        }
        return res.status(200).json(response(user))
    })
}

const delete_user = async(req, res) => {
    const id_user = req.params.id;

    try {
        let user = await User.findById(id_user)
        if(!user){
            return res.status(404).json(not_found('User not found'))
        }
        // Eliminar el assigned de tasks
        await Task.deleteMany({author: id_user})

        // Eliminar de members de Teams
        await Team.deleteMany({members: id_user})

        // Eliminar user
        await User.findByIdAndDelete(id_user)

        return res.status(200).json(response(user,'User removed'))
    } catch (err) {
        console.log(err)
        return res.status(500).json(internal_error())
    }
}

const get_user = async(req, res) => {
    const id_user = req.params.id;

    try {
        let user = await User.findById(id_user);
        if(!user){
            return res.status(404).json(not_found('User not found'))
        }
        return res.status(200).json( response( user ))    
    } catch (error) {
        return res.status(500).json(internal_error())
    }
}

const get_user_tasks = async (req,res)=>{
    const id_user = req.params.id;
    try{
        let tasks = await Task.find({author: id_user})
        return res.status(200).json(response(tasks))
    }catch(err){
        return res.status(500).internal_error()
    }
}

const get_user_team = async(req, res) => {
    const id_user = req.params.id
    try{
        let teams = await User.findOne({_id:id_user},'teams')
        return res.status(200).json(response(teams))
    }catch(err){
        return res.status(500).json(internal_error())
    }
}

module.exports = {
    get_users,
    post_user,
    get_user,
    put_user,
    delete_user,
    get_user_tasks,
    get_user_team
}
