const express = require('express');
require('dotenv').config()
const app = express();
const mongoose = require('./index');    
const body_parse = require('body-parser');

app.use(body_parse.urlencoded({extended:true}));
app.use(express.json());


app.use('/users', require('./routes/userRoute'));
app.use('/tasks', require('./routes/taskRoute'));
app.use('/teams', require('./routes/teamRoute'));

const PORT = process.env.PORT
app.listen(PORT , () => {
    console.log(`Servidor en port ${PORT}`)
})